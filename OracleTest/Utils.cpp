#include "stdafx.h"
#include "Utils.h"

namespace OracleTest
{
	Point findNextPoint(const GeoMap &_map, int _value, int &_startX, int &_startY)
	{
		while (_startX < _map.RowsCount)
		{
			while (_startY < _map.ColumnsCount)
			{
				if (_map.getPointsCount(_startX, _startY) == _value)
				{
					if (_startY < _map.ColumnsCount - 1)
					{
						return Point(_startX, _startY++);
					}
					else
					{
						_startY = 0;
						if (_startX < _map.RowsCount - 1)
						{
							return Point(_startX++, _map.ColumnsCount - 1);
						}
						else
						{
							_startX = _map.RowsCount;
							_startY = _map.ColumnsCount;
							return Point(_map.RowsCount - 1, _map.ColumnsCount - 1);
						}
					}
				}

				++_startY;
			}

			_startY = 0;
			++_startX;
		}

		return Point(_map.RowsCount, _map.ColumnsCount);
	}

	bool checkRectangle(const GeoMap &_map,
						const Point &_topLeft,
						const Point &_topRight,
						const Point &_bottomRight,
						Point &_breakPoint)
	{
		Point contentPoint = _topRight;
		while (contentPoint.getY() >= _topLeft.getY())
		{
			contentPoint.moveLeft();

			Point prevContentPoint = contentPoint;
			while (contentPoint.getX() < _bottomRight.getX())
			{
				contentPoint.moveDown();
				if (_map.getPointsCount(contentPoint.getX(), contentPoint.getY()) != 0)
				{
					_breakPoint = contentPoint;
					return false;
				}
			}

			contentPoint = prevContentPoint;
		}

		return true;
	}

	void updateMaxRect(Rectangle &_rect,
				       int &_maxArea,
				       const Point &_topLeft,
				       const Point &_topRight,
				       const Point &_bottomRight)
	{
		Point bottomLeft(_bottomRight.getX(), _topLeft.getY());
		Rectangle newRect(_topLeft, _topRight, bottomLeft, _bottomRight);
		int newArea = newRect.getArea();
		if (newArea > _maxArea)
		{
			_maxArea = newArea;
			_rect = newRect;
		}
	}

	GeoRectangle createGeoRectangleFromRect(const Rectangle &_rect)
	{
		using coord_t = GeoCoordinate::CoordinateType;

		return GeoRectangle(
			GeoCoordinate(
				static_cast<coord_t>(_rect.TopLeft.getX() - 90),
				static_cast<coord_t>(_rect.TopLeft.getY() - 180)
			),
			GeoCoordinate(
				static_cast<coord_t>(_rect.TopRight.getX() - 90),
				static_cast<coord_t>(_rect.TopRight.getY() - 180 + 1)
			),
			GeoCoordinate(
				static_cast<coord_t>(_rect.BottomLeft.getX() - 90 + 1),
				static_cast<coord_t>(_rect.BottomLeft.getY() - 180)
			),
			GeoCoordinate(
				static_cast<coord_t>(_rect.BottomRight.getX() - 90 + 1),
				static_cast<coord_t>(_rect.BottomRight.getY() - 180 + 1)
			)
		);
	}
}