#pragma once

#include "Point.h"

namespace OracleTest
{
	struct Rectangle
	{
		Rectangle() noexcept
		{
		}

		Rectangle(const Point &_topLeft, const Point &_topRight, const Point &_bottomLeft, const Point &_bottomRight) noexcept
			: TopLeft(_topLeft), TopRight(_topRight), BottomLeft(_bottomLeft), BottomRight(_bottomRight)
		{
		}

		int getArea() const
		{
			return getWidth() * getHeight();
		}

		int getWidth() const
		{
			return TopRight.getY() - TopLeft.getY() + 1;
		}

		int getHeight() const
		{
			return BottomLeft.getX() - TopLeft.getX() + 1;
		}

		Point TopLeft;
		Point TopRight;
		Point BottomLeft;
		Point BottomRight;
	};
}