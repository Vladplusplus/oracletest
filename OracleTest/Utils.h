#pragma once

#include "Point.h"
#include "Rectangle.h"
#include "GeoMap.h"
#include "GeoRectangle.h"

namespace OracleTest
{
	Point findNextPoint(const GeoMap &_map, int _value, int &_startX, int &_startY);
	
	bool checkRectangle(const GeoMap &_map,
						const Point &_topLeft,
						const Point &_topRight,
						const Point &_bottomRight,
						Point &_breakPoint);
	
	void updateMaxRect(Rectangle &_rect,
					   int &_maxArea,
					   const Point &_topLeft,
					   const Point &_topRight,
					   const Point &_bottomRight);

	GeoRectangle createGeoRectangleFromRect(const Rectangle &_rect);

} // namespace OracleTest