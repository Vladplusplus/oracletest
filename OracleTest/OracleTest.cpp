// OracleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GeoCoordinate.h"
#include "GeoCoordinateRandomGenerator.h"
#include "GeoRectangle.h"
#include "GeoMap.h"
#include "Utils.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>
#include <thread>
#include <future>
#include <cassert>

void fillMapWithPoints(OracleTest::GeoMap &_map, int _maxPointsCount, int _threadsCount);
void findBiggestEmptyRectangle(const OracleTest::GeoMap &_map, std::promise<OracleTest::GeoRectangle> _promise);


int main()
{
	std::unique_ptr<OracleTest::GeoMap> map = std::make_unique<OracleTest::GeoMap>();

	fillMapWithPoints(*map, 1000000000, 8);
	std::cout << map->getPointsCount() << " coordinates successfully generated" << std::endl;

	std::promise<OracleTest::GeoRectangle> biggestEmptyRectanglePromise;
	std::future<OracleTest::GeoRectangle> biggestEmptyRectangle = biggestEmptyRectanglePromise.get_future();

	std::thread findBiggestEmptyRectagleWorker(
		findBiggestEmptyRectangle,
		std::cref(*map),
		std::move(biggestEmptyRectanglePromise)
	);
	
	findBiggestEmptyRectagleWorker.detach();
	biggestEmptyRectangle.wait();

	try
	{
		OracleTest::GeoRectangle biggestEmptyRect = biggestEmptyRectangle.get();
		std::cout << biggestEmptyRect;
	}
	catch (const std::exception & ex)
	{
		std::cerr << ex.what();
	}

    return 0;
}


void fillMapWithPoints(OracleTest::GeoMap &_map, int _maxPointsCount, int _threadsCount)
{
	OracleTest::GeoCoordinateRandomGenerator rg;

	auto generator = [&]()
	{
		bool coordAdded = false;

		do 
		{
			coordAdded = _map.addCoordinate(rg.generateCoordinate());
		
		} while (coordAdded);
	};

	_map.setMaxPointsCount(_maxPointsCount);

	if (_threadsCount <= 0)
		_threadsCount = 1;

	std::vector<std::thread> workerThreads(_threadsCount);
	for (auto &thread : workerThreads)
		thread = std::thread(generator);

	for (auto &thread : workerThreads)
		thread.join();

	assert(_map.getPointsCount() == _maxPointsCount);
}


void findBiggestEmptyRectangle(const OracleTest::GeoMap &_map, std::promise<OracleTest::GeoRectangle> _promise)
{
	using namespace OracleTest;

	const Point noPoint = Point(_map.RowsCount, _map.ColumnsCount);
	int startX = 0, startY = 0, maxArea = 0;
	Rectangle maxEmptyRect(noPoint, noPoint, noPoint, noPoint);

	try
	{
		do
		{
			Point topLeft = findNextPoint(_map, 0, startX, startY);
			if (topLeft.getX() != _map.RowsCount)
			{
				Point topRight = topLeft;
				while (topRight.getY() < _map.ColumnsCount - 1)
				{
					topRight.moveRight();
					if (_map.getPointsCount(topRight.getX(), topRight.getY()) != 0)
					{
						topRight.moveLeft();
						break;
					}
				}

				updateMaxRect(maxEmptyRect, maxArea, topLeft, topRight, topRight);

				Point bottomRight = topRight;
				bottomRight.moveDown();

				while (topRight != topLeft && bottomRight.getX() < _map.RowsCount)
				{
					Point breakPoint = noPoint;
					for (int j = topLeft.getY(); j <= bottomRight.getY(); ++j)
					{
						breakPoint = Point(bottomRight.getX(), j);
						if (_map.getPointsCount(breakPoint.getX(), breakPoint.getY()) != 0)
						{
							breakPoint.moveLeft();
							breakPoint = Point(topLeft.getX(), breakPoint.getY());
							topRight = std::max(breakPoint, topLeft);
							bottomRight = topRight;
							break;
						}
					}

					updateMaxRect(maxEmptyRect, maxArea, topLeft, topRight, bottomRight);
					bottomRight.moveDown();
				}
			}

		} while (startX != _map.RowsCount && startY != _map.ColumnsCount);

		auto biggestEmptyGeoRect = createGeoRectangleFromRect(maxEmptyRect);

		_promise.set_value(biggestEmptyGeoRect);
	}
	catch (const std::exception & _ex)
	{
		_promise.set_exception(std::current_exception());
	}
}




