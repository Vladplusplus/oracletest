#pragma once

#include <stdexcept>
#include <string>

namespace OracleTest
{
    /*
     * Value type class which describes geographical coordinate.
     */
    class GeoCoordinate final
    {
    public:
        using CoordinateType = double;

        GeoCoordinate() noexcept;
        GeoCoordinate(CoordinateType _latitude, CoordinateType _longitude);
        GeoCoordinate(const GeoCoordinate &_rhs) noexcept;
        ~GeoCoordinate() noexcept;

        static bool isValidCoordinates(CoordinateType _latitude, CoordinateType _longitude);
        CoordinateType getLatitude() const noexcept;
        CoordinateType getLongitude() const noexcept;

        GeoCoordinate& operator = (const GeoCoordinate &_rhs) noexcept;
        bool operator == (const GeoCoordinate &_rhs) const noexcept;
        bool operator != (const GeoCoordinate &_rhs) const noexcept;
        bool operator < (const GeoCoordinate &_rhs) const noexcept;
        bool operator <= (const GeoCoordinate &_rhs) const noexcept;
        bool operator > (const GeoCoordinate &_rhs) const noexcept;
        bool operator >= (const GeoCoordinate &_rhs) const noexcept;

        friend std::ostream& operator << (std::ostream &_out, const GeoCoordinate &_coord);

    private:
        CoordinateType m_latitude, m_longitude;
    };

    /*
     * Exception class which describes invalid coordinate values
     */
    class InvalidGeoCoordinateException final
        : public std::out_of_range
    {
    public:
        InvalidGeoCoordinateException(GeoCoordinate::CoordinateType _latitude, GeoCoordinate::CoordinateType _longitude);
        ~InvalidGeoCoordinateException();
        
        GeoCoordinate::CoordinateType getLatitude() const noexcept;
        GeoCoordinate::CoordinateType getLongitude() const noexcept;

    private:
        static std::string createMessage(GeoCoordinate::CoordinateType _latitude, GeoCoordinate::CoordinateType _longitude);
        GeoCoordinate::CoordinateType m_latitude, m_longitude;
    };

    /*
     * Helper class with utility functions for GeoCoordinate
     */
    class GeoCoordinateHelper final
    {
    public:
        static bool isValidLatitude(GeoCoordinate::CoordinateType _latitude) noexcept;
        static bool isValidLongitude(GeoCoordinate::CoordinateType _longitude) noexcept;

    private:
        GeoCoordinateHelper() = delete;
        GeoCoordinateHelper(const GeoCoordinateHelper &) = delete;
        GeoCoordinateHelper& operator = (const GeoCoordinateHelper &) = delete;
    };

} // namespace OracleTest
