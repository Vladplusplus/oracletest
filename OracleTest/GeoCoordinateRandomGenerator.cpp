#include "stdafx.h"
#include "GeoCoordinateRandomGenerator.h"
#include <cassert>
#include <type_traits>

namespace OracleTest
{
    GeoCoordinateRandomGenerator::GeoCoordinateRandomGenerator()
        : m_randomDevice()
		, m_generator(m_randomDevice())
		, m_latitudeDistribution(-90.0, 90.0)
		, m_longitudeDistribution(-180.0, 180.0)
    {
    }

    GeoCoordinateRandomGenerator::~GeoCoordinateRandomGenerator()
    {
    }

    GeoCoordinate GeoCoordinateRandomGenerator::generateCoordinate()
    {
		GeoCoordinate::CoordinateType latitude = m_latitudeDistribution(m_generator);
		GeoCoordinate::CoordinateType longitude = m_longitudeDistribution(m_generator);

        assert(GeoCoordinate::isValidCoordinates(latitude, longitude) && "Coordinates is generated incorrectly");
        return GeoCoordinate(latitude, longitude);
    }

} // namespase OracleTest