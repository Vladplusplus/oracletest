#pragma once

namespace OracleTest
{
	class Point
	{
	public:
		Point()
			: m_x(0), m_y(0)
		{
		}

		Point(int _x, int _y)
			: m_x(_x), m_y(_y)
		{
		}

		void moveRight()
		{
			++m_y;
		}

		void moveLeft()
		{
			--m_y;
		}

		void moveUp()
		{
			--m_x;
		}

		void moveDown()
		{
			++m_x;
		}

		int getX() const
		{
			return m_x;
		}

		int getY() const
		{
			return m_y;
		}

		bool operator < (const Point & _rhs) const
		{
			return m_x < _rhs.m_x || (m_x == _rhs.m_x && m_y < _rhs.m_y);
		}

		bool operator == (const Point &_rhs) const
		{
			return m_x == _rhs.m_x && m_y == _rhs.m_y;
		}

		bool operator != (const Point &_rhs) const
		{
			return !((*this) == _rhs);
		}

		bool operator <= (const Point & _rhs) const
		{
			return (*this) < _rhs || (*this) == _rhs;
		}

	private:
		int m_x, m_y;
	};
}