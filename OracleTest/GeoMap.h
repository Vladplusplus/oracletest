#pragma once

#include <atomic>
#include <mutex>
#include "GeoCoordinate.h"

namespace OracleTest
{
	class GeoMap
	{
		struct Node
		{
			std::mutex NodeMutex;
			GeoCoordinate TopLeft;
			GeoCoordinate TopRight;
			GeoCoordinate BottomLeft;
			GeoCoordinate BottomRight;
			int Count = 0;
		};

	public:
		static constexpr int RowsCount = 180;
		static constexpr int ColumnsCount = 360;

		GeoMap();
		~GeoMap();

		void clear();
		bool addCoordinate(const GeoCoordinate &_coord);
		int getPointsCount() const;
		int getPointsCount(int _row, int _column) const;

		void setMaxPointsCount(int _max);

	private:
		void update(Node &_node, const GeoCoordinate &_coord);

		Node m_map[RowsCount][ColumnsCount];
		std::atomic<int> m_size;
		int m_maxPointsCount;
	};

}