#pragma once

#include "GeoCoordinate.h"

namespace OracleTest
{
	class GeoRectangle
	{
	public:
		GeoRectangle();
		GeoRectangle(const GeoCoordinate & _topLeft,
					 const GeoCoordinate & _topRight,
					 const GeoCoordinate & _bottomLeft,
					 const GeoCoordinate & _bottomRigth);

		~GeoRectangle();

		const GeoCoordinate & getTopLeft() const;
		const GeoCoordinate & getTopRight() const;
		const GeoCoordinate & getBottomLeft() const;
		const GeoCoordinate & getBottomRight() const;

		friend std::ostream& operator << (std::ostream& _out, const GeoRectangle &_rect);

	private:
		GeoCoordinate m_topLeft;
		GeoCoordinate m_topRight;
		GeoCoordinate m_bottomLeft;
		GeoCoordinate m_bottomRight;
	};

	class InvaligGeoRectangleException
		: public std::logic_error
	{
	public:
		InvaligGeoRectangleException();
		~InvaligGeoRectangleException();
	};
}