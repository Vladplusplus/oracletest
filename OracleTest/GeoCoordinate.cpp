#include "stdafx.h"
#include "GeoCoordinate.h"

namespace OracleTest
{
    GeoCoordinate::GeoCoordinate() noexcept
        : m_latitude(0.0), m_longitude(0.0)
    {
    }

    GeoCoordinate::GeoCoordinate(CoordinateType _latitude, CoordinateType _longitude)
        : m_latitude(_latitude), m_longitude(_longitude)
    {
        if (!isValidCoordinates(m_latitude, m_longitude))
            throw InvalidGeoCoordinateException(m_latitude, m_longitude);
    }

    GeoCoordinate::GeoCoordinate(const GeoCoordinate &_rhs) noexcept
        : m_latitude(_rhs.m_latitude), m_longitude(_rhs.m_longitude)
    {
    }

    GeoCoordinate::~GeoCoordinate() noexcept
    {
    }

    bool GeoCoordinate::isValidCoordinates(CoordinateType _latitude, CoordinateType _longitude)
    {
        return GeoCoordinateHelper::isValidLatitude(_latitude) && GeoCoordinateHelper::isValidLongitude(_longitude);
    }

    GeoCoordinate::CoordinateType GeoCoordinate::getLatitude() const noexcept
    {
        return m_latitude;
    }

    GeoCoordinate::CoordinateType GeoCoordinate::getLongitude() const noexcept
    {
        return m_longitude;
    }

    GeoCoordinate& GeoCoordinate::operator = (const GeoCoordinate &_rhs) noexcept
    {
        if (&_rhs != this)
        {
            m_latitude = _rhs.m_latitude;
            m_longitude = _rhs.m_longitude;
        }

        return *this;
    }

    bool GeoCoordinate::operator == (const GeoCoordinate &_rhs) const noexcept
    {
        return m_latitude == _rhs.m_latitude && m_longitude == _rhs.m_longitude;
    }

    bool GeoCoordinate::operator != (const GeoCoordinate &_rhs) const noexcept
    {
        return m_latitude != _rhs.m_latitude || m_longitude != _rhs.m_longitude;
    }

    bool GeoCoordinate::operator < (const GeoCoordinate &_rhs) const noexcept
    {
        if (m_latitude < _rhs.m_latitude)
            return true;
        else if (m_latitude == _rhs.m_latitude)
            return m_longitude < _rhs.m_longitude;
        else
            return false;
    }

    bool GeoCoordinate::operator <= (const GeoCoordinate &_rhs) const noexcept
    {
        return !((*this) > _rhs);
    }

    bool GeoCoordinate::operator > (const GeoCoordinate &_rhs) const noexcept
    {
        if (m_latitude > _rhs.m_latitude)
            return true;
        else if (m_latitude > _rhs.m_latitude)
            return m_longitude > _rhs.m_longitude;
        else
            return false;
    }

    bool GeoCoordinate::operator >= (const GeoCoordinate &_rhs) const noexcept
    {
        return !((*this) < _rhs);
    }

    InvalidGeoCoordinateException::InvalidGeoCoordinateException(GeoCoordinate::CoordinateType _latitude, GeoCoordinate::CoordinateType _longitude)
        : std::out_of_range(createMessage(_latitude, _longitude))
        , m_latitude(_latitude), m_longitude(_longitude)
    {
    }

    InvalidGeoCoordinateException::~InvalidGeoCoordinateException()
    {
    }

    GeoCoordinate::CoordinateType InvalidGeoCoordinateException::getLatitude() const noexcept
    {
        return m_latitude;
    }

    GeoCoordinate::CoordinateType InvalidGeoCoordinateException::getLongitude() const noexcept
    {
        return m_longitude;
    }

    std::string InvalidGeoCoordinateException::createMessage(GeoCoordinate::CoordinateType _latitude, GeoCoordinate::CoordinateType _longitude)
    {
        std::string message;
        if (!GeoCoordinateHelper::isValidLatitude(_latitude))
            message += std::string("Latitude value ( ") + std::to_string(_latitude) + std::string(" ) is out of range.\n");

        if(!GeoCoordinateHelper::isValidLongitude(_longitude))
            message += std::string("Longitude value ( ") + std::to_string(_longitude) + std::string(" ) is out of range.\n");

        return message;
    }

    bool GeoCoordinateHelper::isValidLatitude(GeoCoordinate::CoordinateType _latitude) noexcept
    {
        return _latitude >= -90.0 && _latitude <= 90.0;
    }

    bool GeoCoordinateHelper::isValidLongitude(GeoCoordinate::CoordinateType _longitude) noexcept
    {
        return _longitude >= -180.0 && _longitude <= 180.0;
    }

    std::ostream& operator << (std::ostream &_out, const GeoCoordinate &_coord)
    {
        _out << std::string("GeoCoordinate ( ");
        _out << _coord.m_latitude;
        _out << std::string(" : ");
        _out << _coord.m_longitude;
        _out << std::string(" )");
        return _out;
    }

} // namespace OracleTest