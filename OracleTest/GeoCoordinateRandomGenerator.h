#pragma once

#include "GeoCoordinate.h"
#include <random>

namespace OracleTest
{
    /*
     * Helper class for generating random geographical coordinates
     */
    class GeoCoordinateRandomGenerator final
    {
    public:
        GeoCoordinateRandomGenerator();
        ~GeoCoordinateRandomGenerator();

        GeoCoordinate generateCoordinate();

    private:
        GeoCoordinateRandomGenerator(const GeoCoordinateRandomGenerator &) = delete;
        GeoCoordinateRandomGenerator& operator = (const GeoCoordinateRandomGenerator &) = delete;

		std::random_device m_randomDevice;
		std::mt19937 m_generator;
		std::uniform_real_distribution<GeoCoordinate::CoordinateType> m_latitudeDistribution;
		std::uniform_real_distribution<GeoCoordinate::CoordinateType> m_longitudeDistribution;
    };

} // namespace OracleTest