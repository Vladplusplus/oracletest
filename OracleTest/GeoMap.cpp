#include "stdafx.h"
#include "GeoMap.h"
#include "GeoCoordinate.h"

namespace OracleTest
{
	GeoMap::GeoMap()
	{
		clear();
	}

	GeoMap::~GeoMap()
	{
	}

	void GeoMap::clear()
	{
		m_size = 0;
		for (int i = 0; i < RowsCount; ++i)
		{
			for (int j = 0; j < ColumnsCount; ++j)
			{
				m_map[i][j].Count = 0;
				m_map[i][j].TopLeft = GeoCoordinate();
				m_map[i][j].TopRight = GeoCoordinate();
				m_map[i][j].BottomLeft = GeoCoordinate();
				m_map[i][j].BottomRight = GeoCoordinate();
			}
		}
	}

	bool GeoMap::addCoordinate(const GeoCoordinate &_coord)
	{
		GeoCoordinate::CoordinateType latitude = floor(_coord.getLatitude());
		if (latitude == 90)
			latitude = 89;

		GeoCoordinate::CoordinateType longitude = floor(_coord.getLongitude());
		if (longitude == 180)
			longitude = 179;

		int row = 90 + static_cast<int>(latitude);
		int column = 180 + static_cast<int>(longitude);

		bool allowAdd = m_maxPointsCount <= 0;
		allowAdd |= (m_size.fetch_add(1) < m_maxPointsCount);

		if (allowAdd)
		{
			update(m_map[row][column], _coord);
			return true;
		}
		else
		{
			m_size = m_maxPointsCount;
			return false;
		}
	}

	void GeoMap::update(Node &_node, const GeoCoordinate &_coord)
	{
		std::lock_guard<std::mutex> lock(_node.NodeMutex);
		if (_node.Count == 0)
		{
			_node.TopLeft = _coord;
			_node.TopRight = _coord;
			_node.BottomLeft = _coord;
			_node.BottomRight = _coord;
		}
		else
		{
			if (_coord.getLatitude() <= _node.TopLeft.getLatitude() && _coord.getLongitude() < _node.TopLeft.getLongitude())
				_node.TopLeft = _coord;
			
			if (_coord.getLatitude() <= _node.TopRight.getLatitude() && _coord.getLongitude() > _node.TopRight.getLongitude())
				_node.TopRight = _coord;
			
			if (_coord.getLatitude() >= _node.BottomLeft.getLatitude() && _coord.getLongitude() < _node.BottomLeft.getLatitude())
				_node.BottomLeft = _coord;

			if (_coord.getLatitude() >= _node.BottomRight.getLatitude() && _coord.getLongitude() > _node.BottomRight.getLongitude())
				_node.BottomRight = _coord;
		}

		++_node.Count;
	}

	int GeoMap::getPointsCount(int _row, int _column) const
	{
		if (_row < 0 || _row >= RowsCount)
			throw std::out_of_range("row is out of range");
		else if (_column < 0 || _column >= ColumnsCount)
			throw std::out_of_range("column is out of range");
		else
			return m_map[_row][_column].Count;
	}

	int GeoMap::getPointsCount() const
	{
		return m_size;
	}

	void GeoMap::setMaxPointsCount(int _max)
	{
		m_maxPointsCount = _max;
	}

}