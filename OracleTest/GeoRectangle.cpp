#include "stdafx.h"
#include "GeoRectangle.h"

namespace OracleTest
{
	GeoRectangle::GeoRectangle()
	{
	}

	GeoRectangle::GeoRectangle(
		const GeoCoordinate & _topLeft,
		const GeoCoordinate & _topRight,
		const GeoCoordinate & _bottomLeft,
		const GeoCoordinate & _bottomRigth
	)
		: m_topLeft(_topLeft)
		, m_topRight(_topRight)
		, m_bottomLeft(_bottomLeft)
		, m_bottomRight(_bottomRigth)
	{
		if (m_topLeft.getLongitude() != m_bottomLeft.getLongitude())
			throw InvaligGeoRectangleException();
		else if(m_topLeft.getLongitude() > m_topRight.getLongitude())
			throw InvaligGeoRectangleException();

		if (m_topLeft.getLatitude() != m_topRight.getLatitude())
			throw InvaligGeoRectangleException();
		else if(m_topLeft.getLatitude() > m_bottomLeft.getLatitude())
			throw InvaligGeoRectangleException();

		if (m_bottomRight.getLongitude() != m_topRight.getLongitude())
			throw InvaligGeoRectangleException();
		else if(m_bottomRight.getLongitude() < m_bottomLeft.getLatitude())
			throw InvaligGeoRectangleException();

		if (m_bottomRight.getLatitude() != m_bottomLeft.getLatitude())
			throw InvaligGeoRectangleException();
		else if(m_bottomRight.getLatitude() < m_topRight.getLatitude())
			throw InvaligGeoRectangleException();
	}

	GeoRectangle::~GeoRectangle()
	{
	}

	const GeoCoordinate & GeoRectangle::getTopLeft() const
	{
		return m_topLeft;
	}

	const GeoCoordinate & GeoRectangle::getTopRight() const
	{
		return m_topRight;
	}

	const GeoCoordinate & GeoRectangle::getBottomLeft() const
	{
		return m_bottomLeft;
	}

	const GeoCoordinate & GeoRectangle::getBottomRight() const
	{
		return m_bottomRight;
	}

	std::ostream& operator << (std::ostream& _out, const GeoRectangle &_rect)
	{
		_out << "GeoRectangle ( " << _rect.getTopLeft() << " : " << _rect.getTopRight() << " )" << std::endl;
		_out << "             ( " << _rect.getBottomLeft() << " : " << _rect.getBottomRight() << " )";

		return _out;
	}

	InvaligGeoRectangleException::InvaligGeoRectangleException()
		: std::logic_error("Figure is not a rectangle")
	{
	}

	InvaligGeoRectangleException::~InvaligGeoRectangleException()
	{
	}
}